# Iteration Plan decisio%4

## Configuration
  - [x] Staging phase in CI (unit tests in production-like environment) decisio#6
  - [x] Set up staging tests decisio#6
  - [x] Config Rollbacks + Scalability (deployment phase) decisio#6

## Database
  - [ ] Adjust default data structure decisio#16
  - [ ] Code all necessary requests decisio#16
  - [x] Deploy to GKE (Google Kubernetes Engine) decisio#6

## Backend  
  - [ ] Code all necessary endpoints decisio#17
  - [x] JSON Web Tokens / Authentication (from It.1) decisio#11
  - [ ] Code decisions phases logic (first votes, debate, second votes, final decision) decisio#15
  - [ ] Set up unit tests decisio#18

## Frontend Web
  - [ ] Improve static presentation page (from It.1)
  - [x] Login page (from It.1) decisio#11
  - [x] Profile management page (Optional) decisio#20
  - [ ] Teams management page (Optional) decisio#21
  - [x] Decision creation page (from It.1) decisio#13
  - [ ] Improve dashboard
  - [x] Decision page (for a specific decision) decisio#14
  - [ ] Set up unit tests decisio#19

## Frontend Mobile (Optional)
  - [ ] Learn basic React-native concepts
  - [ ] Set up mobile dev environment
  - [ ] Code mobile app backbone