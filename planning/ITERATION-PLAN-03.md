# Iteration Plan decisio%5

## Database
  - [ ] Adjust default data structure decisio#16
  - [ ] Code all necessary requests decisio#16

## Backend  
  - [ ] Code all necessary endpoints decisio#17
  - [ ] Code decisions phases logic (first votes, debate, second votes, final decision) decisio#15
  - [ ] Set up unit tests decisio#18

## Frontend Web
  - [ ] Improve homepage
  - [ ] Improve profile management page
  - [ ] Teams management page decisio#21
  - [ ] Organization management page
  - [ ] Invitation in an organization
  - [ ] Improve decision creation page
  - [ ] Improve dashboard
  - [ ] Decisions phases (first votes, debate, second votes, final decision)
  - [ ] Set up unit tests decisio#19

## Frontend Mobile (Optional)
  - [ ] Learn basic React-native concepts
  - [ ] Set up mobile dev environment
  - [ ] Code mobile app backbone
