import React from 'react'
import { Switch, Route } from 'react-router-dom'
import * as routes from '../constants/routes'
import AuthUserContext from './AuthUserContext'
import Home from './pages/Home'
import SignUp from './pages/SignUp'
import SignIn from './pages/SignIn'
import Profile from './pages/Profile'
import PasswordForget from './pages/PasswordForget'
import PasswordChange from './pages/PasswordChange'
import Board from './pages/Board'
import Decision from './pages/Decision'
import EditProfile from './pages/EditProfile'
import DecisionCreation from './pages/DecisionCreation'

const Main = () => (
  <div>
    <AuthUserContext.Consumer>
      {
        ({ collaborator, idToken }) => (
          <Switch>
            <Route exact path={routes.HOME} component={Home} />
            <Route exact path={routes.SIGN_UP} component={SignUp} />
            <Route exact path={routes.SIGN_IN} component={SignIn} />
            <Route exact path={routes.PASSWORD_FORGET} component={PasswordForget} />
            <Route exact path={routes.PASSWORD_CHANGE} component={PasswordChange} />
            <Route
              exact
              path={routes.BOARD}
              render={() => <Board userId={collaborator._id} idToken={idToken} />}
            />
            <Route
              exact
              path={routes.NEW_DECISION}
              render={() => <DecisionCreation userId={collaborator._id} idToken={idToken} />}
            />
            <Route
              path={`${routes.DECISION}/:id`}
              render={() => <Decision idToken={idToken} />}
            />
            <Route
              path={`${routes.PROFILE}/:id`}
              render={() => <Profile idToken={idToken} />}
            />
            <Route
              exact
              path={routes.EDITPROFILE}
              render={() => <EditProfile collaborator={collaborator} idToken={idToken} />}
            />
          </Switch>
        )
      }
    </AuthUserContext.Consumer>
  </div>
)

export default Main
