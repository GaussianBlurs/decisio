import React, { Component } from 'react'
import { Container, Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap'
import { auth } from '../../firebase'

const byPropKey = (propertyName, value) => () => ({
  [propertyName]: value
})

const INITIAL_STATE = {
  email: '',
  error: null,
  verticalPos: 0,
  mainHeight: 0
}

class ForgotPasswordPage extends Component {
  constructor(props) {
    super(props)
    this.state = { ...INITIAL_STATE }
  }

  componentDidMount() {
    window.addEventListener('resize', this.updateDimensions)
    this.updateDimensions()
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateDimensions)
    document.body.style.overflow = 'scroll'
  }

  updateDimensions = () => {
    const headerHeight = 80
    const windowHeight = window.innerHeight
    const containerHeight = this.container.clientHeight

    // Set container vertical position and main container height
    const verticalPos = Math.max((windowHeight / 2) - (containerHeight / 2), headerHeight + 50)
    const mainHeight = Math.max(window.innerHeight - headerHeight, containerHeight + 100)
    // Enable or disable scroll
    if (verticalPos > headerHeight + 50) {
      document.body.style.overflow = 'hidden'
    } else {
      document.body.style.overflow = 'scroll'
    }

    // State
    this.setState({ verticalPos, mainHeight })
  }

  onSubmit = (event) => {
    const { email } = this.state

    auth.doPasswordReset(email)
      .then(() => {
        this.setState({ ...INITIAL_STATE })
      })
      .catch((error) => {
        this.setState(byPropKey('error', error))
      })

    event.preventDefault()
  }

  render() {
    const {
      email,
      error,
      verticalPos,
      mainHeight
    } = this.state

    const isInvalid = email === ''

    return (
      <div
        className="main-dark"
        style={{ minHeight: `${mainHeight}px` }}
      >
        <Container>
          <div
            ref={(elem) => { this.container = elem }}
            className="sign-form-container justify-content-center"
            style={{ top: `${verticalPos}px` }}
          >
            <h1 className="text-center">Forgot Password</h1>
            <Form onSubmit={this.onSubmit}>
              { error && <Alert color="dark-danger" className="mt-3">{error.message}</Alert> }
              <FormGroup>
                <Label for="forgotEmail">EMAIL</Label>
                <Input
                  id="forgotEmail"
                  value={email}
                  onChange={event => this.setState(byPropKey('email', event.target.value))}
                  type="email"
                  placeholder="Email Address"
                  className="sign-form"
                />
              </FormGroup>
              <Button color="brand" disabled={isInvalid} type="submit" className="action-btn">
                Reset My Password
              </Button>
            </Form>
          </div>
        </Container>
      </div>
    )
  }
}

export default ForgotPasswordPage
