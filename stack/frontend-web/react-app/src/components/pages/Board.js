import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { Container, Row, Col, Nav, NavItem, NavLink, TabContent, TabPane, Button } from 'reactstrap'
import classnames from 'classnames'
import * as routes from '../../constants/routes'
import Decisions from '../board/Decisions'
import withAuthorization from '../withAuthorization'

import '../../assets/scss/Board.scss'

class Board extends React.Component {
  static propTypes = {
    userId: PropTypes.string.isRequired,
    idToken: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      activeTab: 'Open'
    }
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      })
    }
  }

  render() {
    return (
      <Container className="mt-4">
        <div className="clearfix">
          <h1 className="float-left">Board</h1>
          <Button color="success" className="mb-2 float-right" tag={Link} to={routes.NEW_DECISION}>New decision</Button>
        </div>
        <hr />
        <Row>
          <Col>
            <Nav tabs>
              <NavItem>
                <NavLink
                  id="open-tab"
                  className={classnames({ active: this.state.activeTab === 'Open' })}
                  onClick={() => { this.toggle('Open') }}
                >
                  Open
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  id="closed-tab"
                  className={classnames({ active: this.state.activeTab === 'Closed' })}
                  onClick={() => { this.toggle('Closed') }}
                >
                  Closed
                </NavLink>
              </NavItem>
            </Nav>
          </Col>
        </Row>
        <Row>
          <TabContent style={{ width: '100%' }} activeTab={this.state.activeTab}>
            <TabPane tabId="Open">
              <Decisions userId={this.props.userId} idToken={this.props.idToken} status="Open" />
            </TabPane>
            <TabPane tabId="Closed">
              <Decisions userId={this.props.userId} idToken={this.props.idToken} status="Closed" />
            </TabPane>
          </TabContent>
        </Row>
      </Container>
    )
  }
}

const authCondition = authUser => !!authUser

export default withAuthorization(authCondition)(Board)
