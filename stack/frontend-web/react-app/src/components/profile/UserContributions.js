import React from 'react'
import PropTypes from 'prop-types'
import { Alert } from 'reactstrap'
import axios from '../../axios'
import withAuthorization from '../withAuthorization'
import ContributionsListItem from '../decision/ContributionsListItem'
import PagePagination from '../utils/Pagination'

class UserContributions extends React.Component {
  static propTypes = {
    idToken: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      error: null,
      hasLoaded: false,
      contributions: [],
      page: 1,
      pageSize: 5,
      hasPreviousPage: false,
      hasNextPage: false
    }
  }

  componentDidMount = () => {
    this.fetchContributions()
  }

  fetchContributions = () => {
    const { props } = this
    return axios.get(`/contributions/user/${props.userId}?page=${this.state.page}&pageSize=${this.state.pageSize}`, { headers: { Authorization: `Bearer ${this.props.idToken}` } })
      .then(response => this.setState(prevState => ({
        contributions: response.data,
        total: response.headers.pages,
        hasPreviousPage: prevState.page > 1,
        hasNextPage: (prevState.page * prevState.pageSize) < response.headers.pages
      })))
      .then(() => {
        this.setState({
          hasLoaded: true
        })
      })
      .catch(error => this.setState({
        error
      }))
  }

  onPreviousClick = () => {
    this.setState(prevState => ({
      page: prevState.page - 1
    }), this.fetchContributions)
  }

  onNextClick = () => {
    this.setState(prevState => ({
      page: prevState.page + 1
    }), this.fetchContributions)
  }

  render() {
    const { error, hasLoaded, contributions } = this.state
    if (error) {
      return (
        <Alert color="dark-danger" className="mt-3">{error.message}</Alert>
      )
    }
    if (hasLoaded) {
      return (
        <div ref={(elem) => { this.container = elem }}>
          { contributions.map(contribution => (
            <ContributionsListItem
              key={contribution._id}
              contribution={contribution}
              idToken={this.props.idToken}
            />
          )) }
          <PagePagination
            onPreviousClick={this.onPreviousClick}
            onNextClick={this.onNextClick}
            hasPreviousPage={this.state.hasPreviousPage}
            hasNextPage={this.state.hasNextPage}
          />
        </div>
      )
    }
    return (
      <div>Loading...</div>
    )
  }
}

const authCondition = authUser => !!authUser

export default withAuthorization(authCondition)(UserContributions)
