import React from 'react'
import { Link } from 'react-router-dom'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap'
import * as routes from '../../constants/routes'
import AuthUserContext from '../AuthUserContext'
import UserDropDown from './UserMenuDropDown'
import logo from '../../assets/images/logo.svg'
import '../../assets/scss/Header.scss'

const AuthNav = () => (
  <React.Fragment>
    <Nav navbar>
      <NavItem>
        <NavLink tag={Link} to={routes.BOARD}>Board</NavLink>
      </NavItem>
    </Nav>
    <Nav navbar className="ml-auto">
      <NavItem>
        <UserDropDown />
      </NavItem>
    </Nav>
  </React.Fragment>
)

const NonAuthNav = () => (
  <React.Fragment>
    <Nav navbar className="ml-auto">
      <NavItem>
        <NavLink tag={Link} to={routes.SIGN_UP}>Sign Up</NavLink>
      </NavItem>
    </Nav>
    <Nav navbar className="ml-2">
      <NavItem>
        <Link to={routes.SIGN_IN} className="btn btn-brand">Sign In</Link>
      </NavItem>
    </Nav>
  </React.Fragment>
)

class Navigation extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      collapsed: true
    }
  }

  toggleNavbar = () => {
    this.setState(prevState => ({ collapsed: !prevState.collapsed }))
  }

  render() {
    return (
      <Navbar className="custom-nav1" dark expand="md">
        <NavbarBrand to={routes.HOME} tag={Link}>
          <span><img src={logo} className="navbar-logo mr-3" alt="logo" /></span>
          <span>decisio</span>
        </NavbarBrand>
        <NavbarToggler onClick={this.toggleNavbar} className="mr-2" />
        <Collapse isOpen={!this.state.collapsed} navbar>
          <AuthUserContext.Consumer>
            { ({ authUser, collaborator }) => (authUser && collaborator ?
              <AuthNav /> : <NonAuthNav />)}
          </AuthUserContext.Consumer>
        </Collapse>
      </Navbar>
    )
  }
}

const Header = () => (
  <div>
    <Navigation />
  </div>
)

export default Header
