import React from 'react'
import { Link } from 'react-router-dom'
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'
import AuthUserContext from '../AuthUserContext'
import * as routes from '../../constants/routes'
import { auth } from '../../firebase'
import userImage from '../../assets/images/user.png'

export default class UserDropDown extends React.Component {
  constructor(props) {
    super(props)

    this.toggle = this.toggle.bind(this)
    this.state = {
      dropdownOpen: false
    }
  }

  toggle() {
    this.setState(prevState => ({
      dropdownOpen: !prevState.dropdownOpen
    }))
  }

  render() {
    return (
      <Dropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle caret>
          <span><img src={userImage} className="navbar-default mr-1" height="20" alt="logo" /></span>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem header>
            <AuthUserContext.Consumer>
              { ({ collaborator }) => (<span>{collaborator.email}</span>)}
            </AuthUserContext.Consumer>
          </DropdownItem>
          <AuthUserContext.Consumer>
            { ({ collaborator }) => (
              <DropdownItem to={`${routes.PROFILE}/${collaborator._id}`} tag={Link}>Profile</DropdownItem>
            )}
          </AuthUserContext.Consumer>
          <DropdownItem divider />
          <DropdownItem to={routes.SETTINGS} tag={Link}>Settings</DropdownItem>
          <DropdownItem onClick={auth.doSignOut}>Sign Out</DropdownItem>
        </DropdownMenu>
      </Dropdown>
    )
  }
}
