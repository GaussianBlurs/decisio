import React from 'react'
import PropTypes from 'prop-types'
import CommentsListItem from './CommentsListItem'

const Comments = (props) => {
  const { comments, idToken } = props

  return (
    <div>
      { comments.map(comment => (
        // eslint-disable-next-line react/no-array-index-key
        <CommentsListItem key={comment._id} comment={comment} idToken={idToken} />
      )) }
    </div>
  )
}

Comments.propTypes = {
  comments: PropTypes.arrayOf(
    PropTypes.shape({
      author: PropTypes.shape({
        name: PropTypes.string.isRequired,
        _id: PropTypes.string.isRequired
      }).isRequired,
      text: PropTypes.string.isRequired,
      date: PropTypes.string.isRequired,
      points: PropTypes.number.isRequired,
      _id: PropTypes.string.isRequired
    }).isRequired
  ).isRequired,
  idToken: PropTypes.string.isRequired
}

export default Comments
