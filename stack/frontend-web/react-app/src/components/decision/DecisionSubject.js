import React from 'react'
import PropTypes from 'prop-types'
import { Badge } from 'reactstrap'
import Accordion from '../utils/Accordion'
import OptionVoteButton from './utils/OptionVoteButton'

const DecisionSubject = (props) => {
  const { decision } = props
  return (
    <div>
      <div className="mb-2 clearfix">
        <h1 className="d-inline vertical-align mr-2">{decision.title}</h1>
        <Badge
          color={decision.state === 'Open' ? 'success' : 'danger'}
          className="d-inline"
        >
          {decision.state}
        </Badge>
        <div className="d-inline-block float-right">
          <div className="d-inline vertical-align">Teams:</div>
          { decision.teams.map( team => (
            <Badge
              key={team._id}
              color="primary"
              className="d-inline ml-2 vertical-align"
              pill
            >
              {team.name}
            </Badge>
          )) }
        </div>
      </div>
      <div className="text-wrap mt-3 mb-4">
        {decision.description}
      </div>
      <div className="my-3">
        <Accordion>
          { decision.options.map(option => (
            <Accordion.Item key={option._id}>
              <Accordion.Header>
                {option.title}
              </Accordion.Header>
              <Accordion.Body>
                <div className="clearfix">
                  <div className="mb-3">
                    {option.description}
                  </div>
                  <div className="float-right">
                    <OptionVoteButton id={option._id} />
                  </div>
                </div>
              </Accordion.Body>
            </Accordion.Item>
          )) }
        </Accordion>
      </div>
    </div>
  )
}

DecisionSubject.propTypes = {
  decision: PropTypes.shape({
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    state: PropTypes.string.isRequired,
    teams: PropTypes.arrayOf(
      PropTypes.shape({
        _id: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired
      }).isRequired
    ).isRequired,
    options: PropTypes.arrayOf(
      PropTypes.shape({
        title: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired
      }).isRequired
    ).isRequired
  }).isRequired
}

export default DecisionSubject
