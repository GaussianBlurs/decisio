import React from 'react'
import PropTypes from 'prop-types'
import { Alert } from 'reactstrap'
import InfiniteScroll from 'react-infinite-scroll-component'
import axios from '../../axios'
import withAuthorization from '../withAuthorization'
import ContributionsListItem from './ContributionsListItem'
import Spinner from '../utils/Spinner'

class Contributions extends React.Component {
  static propTypes = {
    idToken: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      error: null,
      contributions: [],
      page: 1,
      pageSize: 5,
      hasMoreElements: false
    }
  }

  componentDidMount = () => {
    this.fetchContributions()
  }

  fetchContributions = () => {
    const { props } = this
    return axios.get(`/contributions/decision/${props.decisionId}?page=${this.state.page}&pageSize=${this.state.pageSize}`,
      { headers: { Authorization: `Bearer ${this.props.idToken}` } })
      .then(response => this.setState(prevState => ({
        contributions: [...prevState.contributions, ...response.data],
        total: response.headers.pages,
        hasMoreElements: (prevState.page * prevState.pageSize) < response.headers.pages
      })))
      .then(() => {
        if (window.innerHeight >
            this.container.getBoundingClientRect().top + this.container.clientHeight &&
            this.state.hasMoreElements) {
          this.fetchMore(this.fetchMore)
        }
      })
      .catch(error => this.setState({
        error
      }))
  }

  fetchMore = () => {
    this.setState(prevState => ({
      page: prevState.page + 1
    }), this.fetchContributions)
  }

  refresh = () => {
    this.setState(({
      contributions: []
    }), this.fetchContributions)
  }

  render() {
    const { error, contributions } = this.state
    if (error) {
      return (
        <Alert color="danger" className="mt-3">{error.message}</Alert>
      )
    }
    return (
      <div ref={(elem) => { this.container = elem }}>
        <InfiniteScroll
          dataLength={this.state.contributions.length}
          next={this.fetchMore}
          hasMore={this.state.hasMoreElements}
          loader={<Spinner />}
        >
          { contributions.map(contribution => (
            <ContributionsListItem
              key={contribution._id}
              contribution={contribution}
              idToken={this.props.idToken}
            />
          )) }
        </InfiniteScroll>
      </div>
    )
  }
}

const authCondition = authUser => !!authUser

export default withAuthorization(authCondition)(Contributions)
