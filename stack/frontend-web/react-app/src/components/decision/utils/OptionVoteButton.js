import React from 'react'
import PropTypes from 'prop-types'
import { Button } from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class OptionVoteButton extends React.Component {
  static propTypes = {
    id: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      isMouseInside: false
    }
  }

  mouseEnter = () => {
    this.setState({ isMouseInside: true })
  }

  mouseLeave = () => {
    this.setState({ isMouseInside: false })
  }

  render() {
    const { isMouseInside } = this.state
    const items = isMouseInside ? (<span className="ml-2">VOTE</span>) : (<span />)

    return (
      <Button
        key={this.props.id}
        onMouseEnter={this.mouseEnter}
        onMouseLeave={this.mouseLeave}
        className="option-vote-btn"
      >
        <FontAwesomeIcon icon="check" />
        { items }
      </Button>
    )
  }
}

export default OptionVoteButton
