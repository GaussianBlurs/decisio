#!/bin/bash
echo 'Coucou ******************************'
set -e

mongo <<EOF
conn = new Mongo()
db = conn.getDB('$MONGODB_DATABASE')
db.auth('$MONGODB_USERNAME', '$MONGODB_PASSWORD')
db.dropDatabase()
db.createCollection('organizations')
db.createCollection('collaborators')
db.createCollection('decisions')
db.createCollection('teams')
db.createCollection('contributions')
db.createCollection('tags')

org1 = ObjectId()
org2 = ObjectId()
org3 = ObjectId()

db.organizations.insertMany(
    [
        {
            _id: org1,
            name:'Lake central'
        },
        {
            _id: org2,
            name:'Pear and co'
        },
        {
            _id: org3,
            name:'Gama pak'
        }
    ]
)

col1 = ObjectId()
col2 = ObjectId()
col3 = ObjectId()
col4 = ObjectId()
col5 = ObjectId()
col6 = ObjectId()
col7 = ObjectId()
col8 = ObjectId()
col9 = ObjectId()
col10 = ObjectId()

db.collaborators.insertMany(
    [
        {
            _id:col1,
            name:'Anthony Lanner',
            birthday: new Date(),
            organization:org1,
            position:"CTO",
            username:"admin",
            password:"aaaaaaa",
            email:"admin@decisio.com",
            isAdmin: true,
            firebaseId: "CF91ZHD1ZLTBEorP1IDqpHXgxZK2"
        },
        {
            _id:col2,
            name:'Particia Kales',
            birthday: new Date(),
            organization:org1,
            position:"Developper",
            username:"pat",
            password:"aaaaaaa",
            email:"patricia@gmail.com",
            isAdmin: false,
            firebaseId: "WxkcVTvjZAZANJFZDioggimERPe2"
        },
        {
            _id:col3,
            name:'Larry Nuffard',
            birthday: new Date(),
            organization:org1,
            position:"Technician",
            username:"larry",
            password:"aaaaaaa",
            email:"larry@gmail.com",
            isAdmin: false,
            firebaseId: "gjI8KQtsWyeGsvfJSIJAkjHHlPH2"
        },
        {
            _id:col4,
            name:'Lucas Scofield',
            birthday: new Date(),
            organization:org1,
            position:"CEO",
            username:"lucas",
            password:"aaaaaaa",
            email:"lucas@gmail.com",
            isAdmin: false,
            firebaseId: "iOCc483pZ4T0uNaompkPQ8yeaZY2"
        },
        {
            _id:col5,
            name:'Lara Jones',
            birthday: new Date(),
            organization:org2,
            position:"Product owner",
            username:"lara",
            password:"aaaaaaa",
            email:"lara@gmail.com",
            isAdmin: false,
            firebaseId: "ppL21OnPV7REPuhB94wgy3wN6IZ2"
        },
        {
            _id:col6,
            name:'Henry McMillan',
            birthday: new Date(),
            organization:org3,
            position:"Developper",
            username:"henry",
            password:"aaaaaaa",
            email:"henry@gmail.com",
            isAdmin: false,
            firebaseId: "X75wlHajIJQex3Z5ZA90fX4lopG2"
        },
        {
            _id:col7,
            name:'Leonard siegethaler',
            birthday: new Date(),
            organization:org3,
            position:"Designer",
            username:"leonard",
            password:"aaaaaaa",
            email:"leonard@gmail.com",
            isAdmin: false,
            firebaseId: "xhQX44iF6EhgFYPhudlpCDm64jJ3"
        },
        {
            _id:col8,
            name:'Mary Nessel',
            birthday: new Date(),
            organization:org3,
            position:"Architect",
            username:"mary",
            password:"aaaaaaa",
            email:"mary@gmail.com",
            isAdmin: false,
            firebaseId: "7fFKXMpVuoSDwOeJW6RjdqGRD6a2"
        },
        {
            _id:col9,
            name:'Bernard Hagelbrot',
            birthday: new Date(),
            organization:org3,
            position:"Director assistant",
            username:"bernard",
            password:"aaaaaaa",
            email:"bernard@gmail.com",
            isAdmin: false,
            firebaseId: "9H85qwKpnrUZGlZoBra3bpq8LCP2"
        }
        {
            _id:col10,
            name:'Test Bot',
            birthday: new Date(),
            organization:org1,
            position:"CTO",
            username:"test",
            password:"1234567",
            email:"test.account@gmail.com",
            isAdmin: true,
            firebaseId: "Iyhe20nX1bWNxPm48D88DWPMu3d2"
        },
    ]
)

team1 = ObjectId()
team2 = ObjectId()
team3 = ObjectId()

db.teams.insertMany(
    [
        {
            _id:team1,
            name:"IT",
            organization:org1,
            participants: [
                {
                    collaborator:col1,
                    role:"Supervisor"
                },
                {
                    collaborator:col2,
                    role:"Contributor"
                },
                {
                    collaborator:col3,
                    role:"Concerned"
                }
            ]
        },
        {
            _id:team2,
            name:"Direction",
            organization:org1,
            participants: [
                {
                    collaborator:col3,
                    role:"Supervisor"
                },
                {
                    collaborator:col1,
                    role:"Contributor"
                }
            ]
        },
        {
            _id:team3,
            name:"QA",
            organization:org1,
            participants: [
                {
                    collaborator:col3,
                    role:"Supervisor"
                }
            ]
        }
    ]
)

tag1 = ObjectId()
tag2 = ObjectId()
tag3 = ObjectId()

db.tags.insertMany(
    [
        {
            _id: tag1,
            title: "HR",
            color: "#42b3f4"
        },
        {
            _id: tag2,
            title: "IT",
            color: "#f4ac41"
        },
        {
            _id: tag3,
            title: "Internet",
            color: "#2e964c"
        }
    ]
)

dec1 = ObjectId();
dec2 = ObjectId();
dec3 = ObjectId();
dec4 = ObjectId();
dec5 = ObjectId();
dec6 = ObjectId();
dec7 = ObjectId();
dec8 = ObjectId();
dec9 = ObjectId();
dec10 = ObjectId();

opt1 = ObjectId()
opt2 = ObjectId()
opt3 = ObjectId()
opt4 = ObjectId()
opt5 = ObjectId()
opt6 = ObjectId()
opt7 = ObjectId()
opt8 = ObjectId()
opt9 = ObjectId()
opt10 = ObjectId()

db.decisions.insertMany(
    [
        {
            _id: dec1,
            title:"Must we fire Brian?",
            description:"He's a slow worker, not very nice looking and he smells like a wet firework.",
            author: col1,
            views: 12,
            lastUpdate: new Date(),
            date:new Date(),
            state:"Closed",
            teams: [team1],
            options: [
                {
                    _id:opt1,
                    title:"Yes",
                    description:"We must fire him immediatly!",
                    votes:2
                },
                {
                    _id:opt2,
                    title:"No",
                    description:"Maybe one last chance?",
                    votes:1
                }
            ],
            tags: [tag1]
        },
        {
            _id:dec2,
            title:"Which browser should we use in our company?",
            description:"We have to choose between 3 very good browsers. Which one will you pick?",
            author: col5,
            views: 32,
            lastUpdate: new Date(),
            state:"Open",
            date:new Date(),
            teams: [team2],
            options: [
                {
                    _id:opt3,
                    title:"Firefox",
                    description:"Open source and fast",
                    votes:1
                },
                {
                    _id:opt4,
                    title:"Chrome",
                    description:"Fast and well known",
                    votes:1
                },
                {
                    _id:opt5,
                    title:"Netscape",
                    description:"Old and not very well known but maybe good?",
                    votes:0
                }
            ],
            tags: [tag2, tag3]
        },
        {
            _id: dec3,
            title:"The end of times",
            description:"He's a slow worker, not very nice looking and he smells like a wet firework.",
            author: col1,
            views: 12,
            lastUpdate: new Date(),
            date:new Date(),
            state:"Open",
            teams: [team1],
            options: [
                {
                    _id:opt1,
                    title:"Yes",
                    description:"We must fire him immediatly!",
                    votes:2
                },
                {
                    _id:opt2,
                    title:"No",
                    description:"Maybe one last chance?",
                    votes:1
                }
            ],
            tags: [tag1]
        },
        {
            _id: dec4,
            title:"Another round for Tim?",
            description:"He's a slow worker, not very nice looking and he smells like a wet firework.",
            author: col1,
            views: 12,
            lastUpdate: new Date(),
            date:new Date(),
            state:"Open",
            teams: [team1],
            options: [
                {
                    _id:opt1,
                    title:"Yes",
                    description:"We must fire him immediatly!",
                    votes:2
                },
                {
                    _id:opt2,
                    title:"No",
                    description:"Maybe one last chance?",
                    votes:1
                }
            ],
            tags: [tag1]
        },
        {
            _id: dec5,
            title:"My face burns",
            description:"He's a slow worker, not very nice looking and he smells like a wet firework.",
            author: col1,
            views: 12,
            lastUpdate: new Date(),
            date:new Date(),
            state:"Open",
            teams: [team1],
            options: [
                {
                    _id:opt1,
                    title:"Yes",
                    description:"We must fire him immediatly!",
                    votes:2
                },
                {
                    _id:opt2,
                    title:"No",
                    description:"Maybe one last chance?",
                    votes:1
                }
            ],
            tags: [tag1]
        },
        {
            _id: dec6,
            title:"Under the sink",
            description:"He's a slow worker, not very nice looking and he smells like a wet firework.",
            author: col1,
            views: 12,
            lastUpdate: new Date(),
            date:new Date(),
            state:"Open",
            teams: [team1],
            options: [
                {
                    _id:opt1,
                    title:"Yes",
                    description:"We must fire him immediatly!",
                    votes:2
                },
                {
                    _id:opt2,
                    title:"No",
                    description:"Maybe one last chance?",
                    votes:1
                }
            ],
            tags: [tag1]
        },
        {
            _id: dec7,
            title:"From the beginning",
            description:"He's a slow worker, not very nice looking and he smells like a wet firework.",
            author: col1,
            views: 12,
            lastUpdate: new Date(),
            date:new Date(),
            state:"Open",
            teams: [team1],
            options: [
                {
                    _id:opt1,
                    title:"Yes",
                    description:"We must fire him immediatly!",
                    votes:2
                },
                {
                    _id:opt2,
                    title:"No",
                    description:"Maybe one last chance?",
                    votes:1
                }
            ],
            tags: [tag1]
        },
        {
            _id: dec8,
            title:"Why bother?",
            description:"He's a slow worker, not very nice looking and he smells like a wet firework.",
            author: col1,
            views: 12,
            lastUpdate: new Date(),
            date:new Date(),
            state:"Open",
            teams: [team1],
            options: [
                {
                    _id:opt1,
                    title:"Yes",
                    description:"We must fire him immediatly!",
                    votes:2
                },
                {
                    _id:opt2,
                    title:"No",
                    description:"Maybe one last chance?",
                    votes:1
                }
            ],
            tags: [tag1]
        },
        {
            _id: dec9,
            title:"Cold but comfortable",
            description:"He's a slow worker, not very nice looking and he smells like a wet firework.",
            author: col1,
            views: 12,
            lastUpdate: new Date(),
            date:new Date(),
            state:"Open",
            teams: [team1],
            options: [
                {
                    _id:opt1,
                    title:"Yes",
                    description:"We must fire him immediatly!",
                    votes:2
                },
                {
                    _id:opt2,
                    title:"No",
                    description:"Maybe one last chance?",
                    votes:1
                }
            ],
            tags: [tag1]
        },
        {
            _id: dec10,
            title:"My eyes, are they real?",
            description:"He's a slow worker, not very nice looking and he smells like a wet firework.",
            author: col1,
            views: 12,
            lastUpdate: new Date(),
            date:new Date(),
            state:"Open",
            teams: [team1],
            options: [
                {
                    _id:opt1,
                    title:"Yes",
                    description:"We must fire him immediatly!",
                    votes:2
                },
                {
                    _id:opt2,
                    title:"No",
                    description:"Maybe one last chance?",
                    votes:1
                }
            ],
            tags: [tag1]
        },
    ]
)

db.contributions.insertMany(
    [
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col2,
            text:"That guy is the worst!",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: [
                {
                    _id: ObjectId(),
                    date: new Date(),
                    author: col1,
                    points: 3,
                    text: "+1"
                },
                {
                    _id: ObjectId(),
                    date: new Date(),
                    author: col3,
                    points: -6,
                    text: "No. Hitler is the worst."
                }
            ]
        },
        {
            _id:ObjectId(),
            author:col4,
            text: "It's a harsh decision to make but I need my desk to be cleaner so he's got to go.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: [
                {
                    _id: ObjectId(),
                    date: new Date(),
                    author: col5,
                    points: 0,
                    text: "Your desk is in need of a good cleaning indeed"
                },
                {
                    _id: ObjectId(),
                    date: new Date(),
                    author: col5,
                    points: -3,
                    text: "Yeah he's right"
                }
            ]
        },
        {
            _id:ObjectId(),
            author:col3,
            text:"I say let's hang him instead.",
            date:new Date(),
            points: 3,
            option:opt2,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"I like those animals.",
            date:new Date(),
            points: 3,
            option:opt3,
            decision:dec2,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col4,
            text: "This browser has such a cute animal in its logo. That's the obvious choice!",
            date:new Date(),
            points: 3,
            option:opt3,
            decision:dec2,
            comments: [
                {
                    _id: ObjectId(),
                    date: new Date(),
                    author: col5,
                    points: -1,
                    text: "I love those animals too, we should hang out sometimes!"
                },
                {
                    _id: ObjectId(),
                    date: new Date(),
                    author: col6,
                    points: 2,
                    text: "Yes this."
                }
            ]
        },
        {
            _id:ObjectId(),
            author:col3,
            text:"I have Chrome at home so...",
            date:new Date(),
            points: 3,
            option:opt4,
            decision:dec2,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col3,
            text:"Really?",
            date:new Date(),
            points: 3,
            option:opt5,
            decision:dec2,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
        {
            _id:ObjectId(),
            author:col1,
            text:"Yes, he is not a good listener when I need it.",
            date:new Date(),
            points: 3,
            option:opt1,
            decision:dec1,
            comments: []
        },
    ]
)


EOF
