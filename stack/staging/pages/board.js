
const I = actor();

module.exports = {
  // insert your locators and methods here
  action: {
    openTab: '#open-tab',
    closedTab: '#closed-tab',
  },

  first() {
    I.amOnPage('http://web:80/board')
    I.see('Board', 'h1')
    I.click(this.action.closedTab)
    I.click(this.action.openTab)
  },
}
