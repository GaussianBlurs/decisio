# CI/CD
## CI
The CI process is split in 5 stages:
- build
- test
- staging
- release
- deploy

#### Build
Installs the necessary node_modules/ dependencies with `npm run install-prod`
and saves them in the cache of the Runner so that next jobs can use them.

Builds the React app and saves it in an artifact so that the release job can use it.

#### Test
Runs unit test.

#### Staging
Deploys the full app locally to run unit tests, using `docker-compose`.

#### Release
Builds `nginx` and `node` docker images and push them on GCR (Google Container Registry).

#### Deploy
Deploys the services and deployments to GKE (Google Kubernetes Engine).

## CD

## Install Helm/Tiller

**Some of the steps above only need to be done when Tiller is not yet installed in the cluster.**

- Helm is a package manager for Kubernetes (like npm for Node).
  To install Helm :

  ```
  curl -o get_helm.sh https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get
  chmod +x get_helm.sh
  ./get_helm.sh
  ```

- Deploy `ServiceAccount` and `ClusterRoleBinding` to grant the necessary role
  and a permissions to Tiller (the Helm server-side component).   
  See tiller-sa-rbac.yml in /configure directory   
  Ref: https://docs.helm.sh/using_helm/#role-based-access-control

  ```
  # Only when installing Tiller in the cluster
  kubectl apply -f tiller-sa-rbac.yml
  ```

- Then init Helm:
  ```
  # Only when installing Tiller in the cluster
  helm init --service-account tiller

  # When Tiller is already installed in the cluster
  helm init --client-only
  ```
  It will set up any necessary local configuration.

## Cert-manager

**When the Certificate Manager is not yet installed in the cluster.**

- Create `ServiceAccount` and Kubernetes `Secret`
  ```
  NAME=clouddns-service-account
  PROJECT=pdg-decisio
  KEY=cert-manager-key.json

  gcloud iam service-accounts create ${NAME} --display-name=${NAME} --project=${PROJECT}
  gcloud iam service-accounts keys create ./${KEY} --iam-account=${NAME}@${PROJECT}.iam.gserviceaccount.com --project=${PROJECT}
  gcloud projects add-iam-policy-binding ${PROJECT} --member=serviceAccount:${NAME}@${PROJECT}.iam.gserviceaccount.com --role=roles/owner
  kubectl create secret generic ${NAME} --from-file=./${KEY} --namespace=cert-manager
  ```

- Install `cert-manager`

  ```
  # Install
  helm install --name cert-manager --namespace cert-manager stable/cert-manager --set ingressShim.defaultIssuerName=letsencrypt --set ingressShim.defaultIssuerKind=ClusterIssuer

  # Upgrade
  helm upgrade cert-manager stable/cert-manager

  # Delete
  helm del --purge cert-manager
  kubectl get customresourcedefinition
  kubectl delete customresourcedefinition certificates.certmanager.k8s.io clusterissuers.certmanager.k8s.io issuers.certmanager.k8s.io
  ```

- Deploy a `ClusterIssuer`, which will provision Certificates for any Namespace in the cluster.
  ```
  kubectl apply -f cert-manager.yml -n cert-manager
  ```

- Deploy `Certificate`
  ```
  kubectl apply -f certificate.yml
  ```

### Ingress

**When the Ingress is not yet installed in the cluster.**

- Install `nginx-ingress` controller + default backend
  ```
  # Install
  helm install --name RELEASE_NAME stable/nginx-ingress --set rbac.create=true

  # Upgrade
  helm upgrade RELEASE_NAME stable/nginx-ingress

  # Delete
  helm del --purge RELEASE_NAME
  ```
  **It's important to link the new LoadBalancer IP address to the domain name in Cloud DNS configurations.**

- Deploy `nginx-ingress` that contains the routing rules.
  ```
  kubectl apply -f ingress.yml
  ```

### MongoDB

**When the MongoDB is not yet installed in the cluster.**

```
# Install
helm install --name RELEASE_NAME -f mongo-values-production.yml stable/mongodb

# Auto-scaling
kubectl autoscale statefulset RELEASE_NAME --cpu-percent=PERCENTAGE --min=MIN --max=MAX

# Upgrade
helm upgrade RELEASE_NAME stable/mongodb
# OR
helm upgrade RELEASE_NAME -f mongo-values-production.yml stable/mongodb --set=image.tag=TAG

# Delete
helm del --purge RELEASE_NAME
```

### Deployments

- Deploy `Services` and `Pods`
  ```
  # API
  kubectl apply -f api-deploy.yml

  # Frontend
  kubectl apply -f nginx-deploy.yml

  # Add auto-scaling to a deployment
  kubectl autoscale deployment NAME --cpu-percent=PERCENTAGE --min=MIN --max=MAX

  # Set image
  kubectl set image deployment/nginx nginx=IMAGE
  ```

### Troubleshooting:

- If the deployment doesn't work as expected:
  ```
  kubectl get all -n <NAMESPACE>
  kubectl describe <TYPE> <NAME> -n <NAMESPACE>
  kubectl logs <POD-ID> -n <NAMESPACE>
  ```

- If everything runs smoothly and the certificate id issued you should see something like:
```
I1031 20:59:44.309465       1 sync.go:281] Issuing certificate...
I1031 20:59:44.309695       1 logger.go:43] Calling GetOrder
I1031 20:59:44.780604       1 logger.go:58] Calling FinalizeOrder
I1031 20:59:46.127813       1 issue.go:196] successfully obtained certificate: cn="decisio.tech" altNames=[decisio.tech www.decisio.tech api.decisio.tech] url="https://acme-v02.api.letsencrypt.org/acme/order/xxx/xxx"
I1031 20:59:46.139897       1 sync.go:300] Certificate issued successfully
I1031 20:59:46.140232       1 helpers.go:201] Found status change for Certificate "certificate" condition "Ready": "False" -> "True"; setting lastTransitionTime to 2018-10-31 20:59:46.140224277 +0000 UTC m=+637.732974396
I1031 20:59:46.140563       1 sync.go:206] Certificate default/certificate scheduled for renewal in 1438 hours
I1031 20:59:46.146958       1 controller.go:185] certificates controller: Finished processing work item "default/certificate"
I1031 21:00:46.147159       1 controller.go:171] certificates controller: syncing item 'default/certificate'
I1031 21:00:46.147652       1 sync.go:206] Certificate default/certificate scheduled for renewal in 1438 hours
I1031 21:00:46.147687       1 controller.go:185] certificates controller: Finished processing work item "default/certificate"
```
