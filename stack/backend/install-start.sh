#!/bin/sh

echo context = "$1"

if [ "$1" = "staging" ]; then
  npm install --only=production
  npm run staging
else
  npm install --only=production
  npm run serve
fi
