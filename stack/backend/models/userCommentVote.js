const mongoose = require('mongoose')
const findOneOrCreate = require('mongoose-findoneorcreate')

const Schema = mongoose.Schema

const userCommentVote = new Schema({
  user: { type: Schema.Types.ObjectId, ref: 'collaborators' },
  comment: { type: Schema.Types.ObjectId, ref: 'comments' },
  score: Number
})

userCommentVote.plugin(findOneOrCreate)

module.exports = mongoose.model('userCommentVotes', userCommentVote)
