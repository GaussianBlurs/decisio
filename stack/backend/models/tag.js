const mongoose = require('mongoose')

const Schema = mongoose.Schema

const tag = new Schema({
  title: String,
  color: String
})

module.exports = mongoose.model('tags', tag)
