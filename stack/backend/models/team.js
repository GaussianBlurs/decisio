const mongoose = require('mongoose')

const Schema = mongoose.Schema

const team = new Schema({
  name: String,
  organization: { type: Schema.Types.ObjectId, ref: 'organizations' },
  participants: [{
    collaborator: { type: Schema.Types.ObjectId, ref: 'collaborators' },
    role: {
      type: String,
      enum: ['Supervisor', 'Contributor', 'Concerned']
    }
  }]
})

module.exports = mongoose.model('teams', team)
