const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const Schema = mongoose.Schema

const comment = new Schema({
  date: Date,
  author: { type: Schema.Types.ObjectId, ref: 'collaborators' },
  points: Number,
  text: String
})

const contribution = new Schema({
  text: String,
  date: Date,
  points: Number,
  author: { type: Schema.Types.ObjectId, ref: 'collaborators' },
  option: { type: Schema.Types.ObjectId, ref: 'decisions.option' },
  decision: { type: Schema.Types.ObjectId, ref: 'decisions' },
  comments: [comment]
})

contribution.plugin(mongoosePaginate)

module.exports = mongoose.model('contributions', contribution)
