// Imports
const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const admin = require('firebase-admin')
const bodyParser = require('body-parser')
const multer = require('multer')
const serviceAccount = require('./serviceAccount')

const upload = multer({ dest: 'profileImages/' })
// const graphql = require('graphql')
const conf = require('./conf').default
// Get database models
// const Organization = require('../models/organization')
const Decision = require('../models/decision')
const Team = require('../models/team')
const Collaborator = require('../models/collaborator')
const Contribution = require('../models/contribution')
const Organization = require('../models/organization') // eslint-disable-line no-unused-vars
const Tag = require('../models/tag') // eslint-disable-line no-unused-vars
const UserContribVote = require('../models/userContribVote')
const UserCommentVote = require('../models/userCommentVote')

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
})

// Useful variables
const app = express()

const corsOptions = {
  // origin: 'http://example.com',
  exposedHeaders: 'pages',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions))

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

const mongoOpt = {
  useNewUrlParser: true,
  reconnectTries: conf.db.reconnectTries,
  reconnectInterval: conf.db.reconnectInterval
}

const mongoUrl = conf.db.url

// MangoDB connection with retry
const connectWithRetry = () => {
  mongoose.connect(mongoUrl, mongoOpt)
    .then(
      () => {
        console.log('Connected to MongoDB') // eslint-disable-line no-console
      },
      (err) => {
        console.error('Failed to connect to MongoDB', err)
        setTimeout(connectWithRetry, 5000)
      },
    )
}

// Connect to MongoDB
connectWithRetry()

// Get token from header
const getBearerToken = authHeader => authHeader.split(' ')[1]

// verify token with firebase SDK
const verifyTokenAndGetUID = token => admin.auth().verifyIdToken(token)
  .then(decodedToken => decodedToken.uid)
  .catch(error => console.error(error))

// Authorization middleware
const isUserAuthenticated = (req, res, next) => {
  const authHeader = req.headers.authorization
  if (!authHeader) {
    return res.status(403).json({
      status: 403,
      message: 'FORBIDDEN'
    })
  }
  const token = getBearerToken(authHeader)
  if (token) {
    return verifyTokenAndGetUID(token)
      .then(userId => Collaborator.findOne({ firebaseId: userId })
        .then((collaborator) => {
          res.locals.user = collaborator
          next()
        }))
      .catch(() => res.status(401).json({
        status: 401,
        message: 'UNAUTHORIZED'
      }))
  }
  return res.status(403).json({
    status: 403,
    message: 'FORBIDDEN'
  })
}

app.post('/collaborators', isUserAuthenticated, (req, res, next) => {
  Collaborator.create({
    name: req.body.name,
    email: req.body.email,
    firebaseId: req.body.firebaseId
  })
    .then((response) => {
      res.send(response)
    })
    .catch(next)
})

app.get('/collaborators/firebase/:firebaseId', isUserAuthenticated, (req, res, next) => {
  Collaborator.findOne({ firebaseId: req.params.firebaseId })
    .populate('organization')
    .then(result => res.send(result))
    .catch(next)
})

app.get('/collaborators/:id', isUserAuthenticated, (req, res, next) => {
  Collaborator.findById(req.params.id)
    .populate('organization')
    .then(result => res.send(result))
    .catch(next)
})

app.put('/collaborator', isUserAuthenticated, (req, res, next) => {
  Collaborator.findByIdAndUpdate(res.locals.user._id,
    {
      name: req.body.name,
      birthday: req.body.birthday
    })
    .then(res.sendStatus(200))
    .catch(next)
})

app.get('/teams/user/:id', isUserAuthenticated, (req, res, next) => {
  Team.find({ 'participants.collaborator': req.params.id })
    .populate('participants.collaborator')
    .populate('organization')
    .then(result => res.send(result))
    .catch(next)
})

// app.get('/decisions', (req, res, next) => {
//   Decision.find(req.query)
//     .populate('author')
//     .populate('teams')
//     .populate('tags')
//     .then(result => res.send(result))
//     .catch(next)
// })

app.post('/decisions', isUserAuthenticated, (req, res, next) => {
  Decision.create({
    title: req.body.title,
    description: req.body.description,
    author: req.body.userId,
    views: 0,
    lastUpdate: new Date(),
    state: 'Open',
    teams: req.body.teams,
    options: req.body.options.map(option => ({
      title: option.title,
      description: option.description,
      votes: 0
    }))
  })
    .then((response) => {
      res.send(response)
    })
    .catch(next)
})

app.get('/decision/:id', isUserAuthenticated, (req, res, next) => {
  Decision.findById(req.params.id)
    .populate('author')
    .populate('teams')
    .populate('tags')
    .then(result => res.send(result))
    .catch(next)
})

/*
 * Get user's bound decisions with pagination
 * params : id       -> user id
 *          pageSize -> number of decisions to show
 *          page     -> current page
 *          state    -> state of decisions to show
 */
app.get('/decisions/user/:id', isUserAuthenticated, (req, res, next) => {
  Team.find({ 'participants.collaborator': req.params.id })
    .then((teams) => {
      const teamsIds = teams.map(t => t._id)
      const skippedDecisions = (req.query.page - 1) * req.query.pageSize
      Decision.paginate(
        { teams: { $in: teamsIds }, state: req.query.state },
        { offset: skippedDecisions, limit: Number(req.query.pageSize), populate: [{ path: 'author', select: ['name', '_id'] }, 'teams', 'tags'] }
      )
        .then((values) => {
          res.set('pages', values.total)
          return values.docs
        })
        .then(result => res.send(result))
        .catch(next)
    })
})

app.get('/contributions/option/:id', isUserAuthenticated, (req, res, next) => {
  Contribution.find({ option: req.params.id })
    .populate('collaborator')
    .populate('decision')
    .then(result => res.send(result))
    .catch(next)
})

app.get('/contributions/decision/:id', isUserAuthenticated, (req, res, next) => {
  const skippedContributions = (req.query.page - 1) * req.query.pageSize
  Contribution.paginate(
    { decision: req.params.id },
    { offset: skippedContributions, limit: Number(req.query.pageSize), populate: [{ path: 'author', select: 'name' }, { path: 'comments.author', select: 'name' }, 'decision'] }
  )
    .then((values) => {
      res.set('pages', values.total)
      return values.docs
    })
    .then(result => res.send(result))
    .catch(next)
})

app.get('/contributions/user/:id', isUserAuthenticated, (req, res, next) => {
  const skippedContributions = (req.query.page - 1) * req.query.pageSize
  Contribution.paginate(
    { author: req.params.id },
    { offset: skippedContributions, limit: Number(req.query.pageSize), populate: [{ path: 'author', select: 'name' }, { path: 'comments.author', select: 'name' }, 'decision'] }
  )
    .then((values) => {
      res.set('pages', values.total)
      return values.docs
    })
    .then(result => res.send(result))
    .catch(next)
})

app.get('/contributions/decision/:id/count', isUserAuthenticated, (req, res, next) => {
  Contribution.countDocuments({ decision: req.params.id })
    .then(result => res.send({ count: result }))
    .catch(next)
})

const computeContribUpvoteIncrement = (score, value) => {
  if (score === 1 && value === 1) {
    return -1
  }
  if (score === 1 && value === -1) {
    return -2
  }
  if (score === 0 && value === 1) {
    return 1
  }
  if (score === 0 && value === -1) {
    return -1
  }
  if (score === -1 && value === 1) {
    return 2
  } // score === -1 && value === -1
  return 1
}

app.put('/contribution/:id/updatescore', isUserAuthenticated, (req, res, next) => {
  // Checks if valid score
  const scoreIncrement = Number(req.body.value)
  if (Math.abs(scoreIncrement) !== 1) {
    const error = new Error('Invalid value')
    error.status = 400
    next(error)
  } else {
    UserContribVote.findOneOrCreate(
      { user: res.locals.user._id, contribution: req.params.id },
      { user: res.locals.user._id, contribution: req.params.id, score: 0 }
    )
      .then(result => (computeContribUpvoteIncrement(result.score, scoreIncrement)))
      .then((increment) => {
        UserContribVote.findOneAndUpdate({
          user: res.locals.user._id,
          contribution: req.params.id
        },
        { $inc: { score: increment } },
        { new: true })
          .then(userContribVote => (
            Contribution.updateOne({ _id: req.params.id }, { $inc: { points: increment } })
              .then(res.send({ score: userContribVote.score, increment }))
          ))
      })
  }
})

app.get('/uservote/contribution/:contributionId', isUserAuthenticated, (req, res, next) => {
  UserContribVote.findOne({ user: res.locals.user._id, contribution: req.params.contributionId })
    .then((result) => {
      const score = result ? result.score : 0
      res.send({ score })
    })
    .catch(next)
})

const computeCommentUpvoteIncrement = (score, value) => {
  if (score === 1 && value === 1) {
    return -1
  }
  if (score === 0 && value === 1) {
    return 1
  }
  return 1
}

app.put('/comment/:id/updatescore', isUserAuthenticated, (req, res, next) => {
  // Checks if valid score
  const scoreIncrement = Number(req.body.value)
  if (scoreIncrement !== 1) {
    const error = new Error('Invalid value')
    error.status = 400
    next(error)
  } else {
    UserCommentVote.findOneOrCreate(
      { user: res.locals.user._id, comment: req.params.id },
      { user: res.locals.user._id, comment: req.params.id, score: 0 }
    )
      .then(result => (computeCommentUpvoteIncrement(result.score, scoreIncrement)))
      .then((increment) => {
        UserCommentVote.findOneAndUpdate({
          user: res.locals.user._id,
          comment: req.params.id
        },
        { $inc: { score: increment } },
        { new: true })
          .then(userCommentVote => (
            UserCommentVote.updateOne({ _id: req.params.id }, { $inc: { points: increment } })
              .then(res.send({ score: userCommentVote.score, increment }))
          ))
      })
  }
})

app.get('/uservote/comment/:commentId', isUserAuthenticated, (req, res, next) => {
  UserCommentVote.findOne({ user: res.locals.user._id, comment: req.params.commentId })
    .then((result) => {
      const score = result ? result.score : 0
      res.send({ score })
    })
    .catch(next)
})

app.post('/profileimage', upload.single('profileImage'), isUserAuthenticated, (req, res, next) => {
  if (req.file) {
    Collaborator.findByIdAndUpdate(
      res.locals.user._id,
      { $set: { profileImage: req.file } },
      { new: true }
    )
      .then(collaborator => (
        res.send(collaborator)
      ))
  } else {
    const error = new Error('Invalid file')
    error.status = 400
    next(error)
  }
})

/* app.get('/profileimage', isUserAuthenticated, (req, res, next) => {
  const filePath = `profileImages/${req.params.filename}`
  res.setHeader('Content-Type', 'image/gif')
  res.sendFile(filePath)
}) */

// Forward 404 to error handler
app.use((req, res, next) => {
  const error = new Error('Not found')
  error.status = 404
  next(error)
})

// Error handler
app.use((err, req, res, next) => { // eslint-disable-line no-unused-vars
  console.error(err)
  res.status(err.status || 500)
  res.send(err.message)
})

// Server
const server = app.listen(8081, () => {
  const host = server.address().address
  const port = server.address().port
  console.log('Node server listening at http://%s:%s', host, port) // eslint-disable-line no-console
})
