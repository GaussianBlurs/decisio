# decisio

[![pipeline status](https://gitlab.com/psrochat/decisio/badges/ci-cd/pipeline.svg)](https://gitlab.com/psrochat/decisio/commits/ci-cd)

## Useful links
### Planning
* [Iteration 1](/planning/ITERATION-PLAN-01.md)
* [Iteration 2](/planning/ITERATION-PLAN-02.md)

### Deployment
* [GKE Deployment](/stack/deployment/README.md)
